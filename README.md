# Simple File Encryption and Decyption

This is a simple program I wrote as a part of my Golang learning process that encrypts and decrypts a target file.

AES-GCM mode is used for encryption and decryption process.

This program takes a `target_file` as a command line argument.

It can be run as:
`go run main.go target_file`

`The key is not secret` at all in this program. I created this just for learning purposes.

Better approach would be reading key from another file, and other measures should be done to protect the secret key.

A simple demo is shown below:

![Code Demo](demo_enc_dec.gif)
