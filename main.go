package main

import (
	"fmt"
	"encoding/hex"
	"io/ioutil"
	"io"
	"log"
	"os"
	"crypto/rand"
	"crypto/aes"
	"crypto/cipher"
)

func main() {
	//Taking Filename as a command line argument
	args := os.Args

	if len(args) != 2 {

		log.Fatal("[+]!!!!Please provide the target filename as the only argument!!!! \n[:Example:] go run main.go target.png")

	}

	//Open file for reading
	file, err := os.Open(args[1])
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Unreadable - Encrypt and Decrypt Files")
	fmt.Println("[+]Encrypting file[+]")

	//ioutil.ReadAll() reads every bytes
	//Returns a slice of unknown slice
	//Plaintext file
	plainfile, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}

	//256 Bit(32 Byte) Length Key for encryption
	// Opening file with 256 Bit AES KEY
	//key, err := ioutil.ReadFile("key")
	//if err != nil {
	//	log.Fatal(err)
	//}

	// ------------------------------- Change the key to something more secure ----------------------------------------//
	// ------------------------------- Better practice would be reading key from another file -------------------------//
	key, _ := hex.DecodeString("737570657220736563726574206b65792066696e642074686520666c61675f5f")
	//fmt.Println(string(key))

	//Creating block
	block, err := aes.NewCipher(key)
	if err != nil {
		log.Fatal(err)
	}

	//Using AES-GCM
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		log.Fatal(err)
	}

	// Never use more than 2^32 random nonces with a given key because of the risk of repetition.
	// Generating Nonce
	nonce := make([]byte, aesgcm.NonceSize())
	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		log.Fatal(err)
	}

	//Encrypting and writing to a file
	encrypted := aesgcm.Seal(nonce, nonce, plainfile, nil)
	err = ioutil.WriteFile("encrypted_file", encrypted, 0700)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("[+]Done[+]")

	//Decryption Process
	fmt.Println()
	fmt.Println("[+]Decrypting file[+]")

	//Reading encrypted file
	ciphertext, err := ioutil.ReadFile("encrypted_file")
	if err != nil {
		log.Fatal(err)
	}

	//Indicating the nonce value used
	ciphertext = ciphertext[aesgcm.NonceSize():]

	//Getting plaintext, Decrypting File, and Writing to the file
	plaintext, err := aesgcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile("decrypted", plaintext, 0700)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("[+]Done[+]")

}
